import logo from "./logo.svg";
import "./App.css";

import { RealTimeChart } from "./RealTimeChart";
import Live from "./components/Live";

function App() {
  return (
    <div className="App">
      <Live />
    </div>
  );
}

export default App;
