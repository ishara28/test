import React, { Component } from "react";
import Chart from "kaktana-react-lightweight-charts";
import axios from "axios";

import openSocket from "socket.io-client";
const socket = openSocket("http://localhost:8000", {
  transports: ["websocket"],
});

export class Live extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        // alignLabels: true,
        // timeScale: {
        //   rightOffset: 12,
        //   barSpacing: 3,
        //   fixLeftEdge: true,
        //   lockVisibleTimeRangeOnResize: true,
        //   rightBarStaysOnScroll: true,
        //   borderVisible: false,
        //   borderColor: "#fff000",
        //   visible: true,
        //   timeVisible: true,
        //   secondsVisible: false,
        // },
        priceLineWidth: 1,
        timeScale: {
          timeVisible: true,
          secondsVisible: false,
        },
      },
      historicalCandlestickSeries: [],
      candlestickSeries: [],
      myData: [],
      historicalCloseData: [],
      closeData: [],
      myCloseData: [],
    };
  }

  componentDidMount() {
    this.getKlineData();
    this.getCloseData();
  }

  getKlineData = () => {
    axios
      .get(
        "https://api.binance.com/api/v3/klines?symbol=BTCUSDT&interval=1m&limit=1000"
      )
      .then((res) => {
        const cdata = res.data.map((d) => {
          return {
            time: d[0] / 1000,
            open: parseFloat(d[1]),
            high: parseFloat(d[2]),
            low: parseFloat(d[3]),
            close: parseFloat(d[4]),
          };
        });
        this.setState(
          {
            historicalCandlestickSeries: [
              {
                data: cdata,
              },
            ],
          },
          () => this.getSocketKLinetData()
        );
      });
  };

  getCloseData = () => {
    axios
      .get(
        "https://api.binance.com/api/v3/klines?symbol=BTCUSDT&interval=1m&limit=1000"
      )
      .then((res) => {
        const cdata = res.data.map((d) => {
          return {
            time: d[0] / 1000,
            value: parseFloat(d[4]),
          };
        });
        this.setState(
          {
            historicalCloseData: [
              {
                data: cdata,
              },
            ],
          },
          () => this.getSocketCloseData()
        );
      });
  };

  getSocketKLinetData = () => {
    socket.on("KLINE", (pl) => {
      console.log(pl);
      this.setState(
        {
          myData: [...this.state.myData, pl],
        },
        () =>
          this.setState({
            candlestickSeries: [
              {
                data: [
                  ...this.state.historicalCandlestickSeries[0].data,
                  ...this.state.myData,
                ],
              },
            ],
          })
      );
    });
  };

  getSocketCloseData = () => {
    socket.on("KLINE", (pl) => {
      console.log(pl);
      this.setState(
        {
          myCloseData: [
            ...this.state.myCloseData,
            {
              time: pl["time"],
              value: pl["close"],
            },
          ],
        },
        () =>
          this.setState({
            closeData: [
              {
                data: [
                  ...this.state.historicalCloseData[0].data,
                  ...this.state.myCloseData,
                ],
              },
            ],
          })
      );
    });
  };

  

  render() {
    return (
      <React.Fragment>
        <Chart
          options={this.state.options}
          candlestickSeries={this.state.candlestickSeries}
          lineSeries={this.state.closeData}
          autoWidth
          height={550}
          darkTheme={true}
          socket={socket}
          legend="BTC/USDT"
        />
      </React.Fragment>
    );
  }
}
export default Live;

