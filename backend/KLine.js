const express = require("express");
const app = express();
const socket = require("socket.io");
const color = require("colors");
const cors = require("cors");
const { WebsocketClient, DefaultLogger } = require("binance");

app.use(express());

const port = 8000;

app.use(cors());

const API_KEY =
  "0baISLZo7wvww0D2IfjAnIbb678oGmHyNbcDlJBs2MR308O7lS08A9n3kXLBNJCu";
const PRIVATE_KEY =
  "InrQIBW2Rb7Gn4aEjJGksyDEPojeaLSbmCq1hBeZGwDhiYQlXROg7yMcA5NITaoW";

// optionally override the logger
const logger = {
  ...DefaultLogger,
  silly: (...params) => {},
};

var server = app.listen(
  port,
  console.log(`Server is running on the port no: ${port} `.green)
);

const io = socket(server);

const wsClient = new WebsocketClient(
  {
    api_key: API_KEY,
    api_secret: PRIVATE_KEY,
    beautify: true,
  },
  logger
);

// receive raw events
wsClient.on("message", (data) => {
  console.log("raw message received ", JSON.stringify(data, null, 2));
});

// notification when a connection is opened
wsClient.on("open", (data) => {
  console.log("connection opened open:", data.wsKey, data.ws.target.url);
});

// receive formatted events with beautified keys. Any "known" floats stored in strings as parsed as floats.
wsClient.on("formattedMessage", (data) => {
  console.log("formattedMessage: ", data);
  io.sockets.emit("KLINE", {
    time: Math.round(data.kline.startTime / 1000),
    open: parseFloat(data.kline.open),
    high: parseFloat(data.kline.high),
    low: parseFloat(data.kline.low),
    close: parseFloat(data.kline.close),
  });
});

// read response to command sent via WS stream (e.g LIST_SUBSCRIPTIONS)
wsClient.on("reply", (data) => {
  console.log("log reply: ", JSON.stringify(data, null, 2));
});

// receive notification when a ws connection is reconnecting automatically
wsClient.on("reconnecting", (data) => {
  console.log("ws automatically reconnecting.... ", data?.wsKey);
});

// receive notification that a reconnection completed successfully (e.g use REST to check for missing data)
wsClient.on("reconnected", (data) => {
  console.log("ws has reconnected ", data?.wsKey);
});

//   wsClient.subscribeSpotUserDataStream();
//   wsClient.subscribeMarginUserDataStream();
//   wsClient.subscribeIsolatedMarginUserDataStream('BTCUSDT');
//   wsClient.subscribeUsdFuturesUserDataStream();


wsClient.subscribeSpotKline("BTCUSDT", "1m");
